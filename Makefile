KERNEL_VSN=$(word 1,$(subst +, ,$(shell git branch --show-current)))

vmlinuz: .builder
	-docker rm -f alpine-kernel-builder
	docker run --name alpine-kernel-builder alpine-kernel-builder sh -c " \
		mkdir /build/target; \
		make ARCH=x86_64 CC=gcc AWK=mawk KBUILD_BUILD_VERSION=dm-env; \
		make ARCH=x86_64 INSTALL_PATH=/build/target install; \
		"
	docker cp alpine-kernel-builder:/build/target/$@ .

.base: Dockerfile
	docker build --build-arg KERNEL_VSN=$(KERNEL_VSN) \
		--target base -t alpine-kernel-builder-base .
	touch $@

menuconfig: virt.x86_64.config
	docker build --build-arg KERNEL_VSN=$(KERNEL_VSN) \
		--target menuconfig_builder \
		-t alpine-kernel-builder-menuconfig .
	-docker rm -f alpine-kernel-builder-$@
	docker run -ti --name alpine-kernel-builder-$@ \
		alpine-kernel-builder-menuconfig \
		make ARCH=x86_64 menuconfig
	docker cp alpine-kernel-builder-$@:/build/linux/.config virt.x86_64.$@
.PHONY: menuconfig

.builder: virt.x86_64.config .base
	docker build --build-arg KERNEL_VSN=$(KERNEL_VSN) \
		--target kernel-builder -t alpine-kernel-builder .
	touch $@
