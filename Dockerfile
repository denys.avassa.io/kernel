from alpine:3.17 as base
run apk add --no-cache bc bison build-base curl diffutils elfutils-dev \
                       findutils flex linux-headers mawk openssl-dev>3 perl \
                       sed xz zstd
run ln -s /usr/bin/mawk /usr/local/bin/awk
run mkdir -p /usr/src /build
arg KERNEL_VSN
run major_vsn=${KERNEL_VSN%%.*}; \
    case ${KERNEL_VSN} in \
      *.*.*) minor_vsn=${KERNEL_VSN%.*};; \
      *.*) minor_vsn=${KERNEL_VSN};; \
    esac; \
    cd /usr/src; \
    curl -LO https://cdn.kernel.org/pub/linux/kernel/v${major_vsn}.x/linux-${minor_vsn}.tar.xz && \
    curl -LO https://cdn.kernel.org/pub/linux/kernel/v${major_vsn}.x/patch-${KERNEL_VSN}.xz && \
    cd /build; \
    tar xf /usr/src/linux-${minor_vsn}.tar.xz; \
    mv linux-${minor_vsn} linux; \
    cd /build/linux; \
    unxz -c < /usr/src/patch-${KERNEL_VSN}.xz | patch -p1 -N
workdir /build/linux

from base as menuconfig_builder
run apk add --no-cache ncurses-dev
copy virt.x86_64.config .config

from base as kernel-builder
copy virt.x86_64.config .config
